/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplication1;

/**
 *
 * @author jules
 */
public class Liste{

Integer  tete;
Liste queue;

//la seule et unique liste vide pour optimisation
public static Liste listevide=new Liste();

//ne pas l'utiliser pour créer des listes
public Liste(){
}


public static Liste creer(){

return listevide;
}

public boolean estVide(){
    return this==listevide;
//retourne si la liste est vide, ie si c'est la liste vide

}

public void ajoute(Integer element){
//doit mettre element en tete dans la liste
if(this.estVide()){
    tete=element;
    queue=listevide;
}
else{
    queue=this;
    tete=element;
}
}

public Integer tete(){
//retourne la tete
return tete;
}


public Liste queue(){
//retourne la queue
return queue;

}


public int taille(){
//retourne la taille de la liste / le nombre d'elements
Liste temp = queue;
Integer i = 0;
while(temp!=listevide){
    temp=temp.queue();
    i=i+1;

    }
return i;
}

public static void main(String[] args){

Liste L1=creer();
//créer une liste vide de nom L2 
     
Liste L2=creer();
//ajoute 3 a L1
L1.ajoute(3);
        //L1 est est vide ?
if(L1.estVide()){
    System.out.println("L1 est vide");
}
else{
    System.out.println("L1 n'est pas vide");
}
//L2 est elle vide ?
if(L2.estVide()){
    System.out.println("L2 est vide");
}
else{
    System.out.println("L2 n'est pas vide");
}

}



}
